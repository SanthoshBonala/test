//
//  ViewController.swift
//  test
//
//  Created by Bonala,Santhosh on 4/22/18.
//  Copyright © 2018 Bonala,Santhosh. All rights reserved.
//

import UIKit
import Mute

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnclicked(_ sender: UIButton) {
     Mute.shared.isPaused = true
    }
    
}

