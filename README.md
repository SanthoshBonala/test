# GDP

## Introduction

## Pre-Requisites to run the project
 node - 8.12.0
 
## Steps to run the project
* change the directory to ProjectNorthwestThreatre
* Install the dependencies using npm install
* change the directory to frontend
* Install the dependencies using npm install
* Generate the build using npm run build
* change the directory to ProjectNorthwestThreatre
* start the application using npm start
## Developed By
1. Santhosh Bonala
1. Ashwith Gundu
1. Saivarun Ilendula
1. Supraja Kumbham
1. Keerthi Chiduruppa
1. Rahul Reddy Lankala
